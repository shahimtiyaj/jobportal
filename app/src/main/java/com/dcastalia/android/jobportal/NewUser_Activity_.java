package com.dcastalia.android.jobportal;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.dcastalia.android.jobportal.DatabaseHelper.SQLiteHandler;
import com.dcastalia.android.jobportal.Utils.VolleyCustomRequest;
import com.dcastalia.android.jobportal.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import static com.android.volley.VolleyLog.d;
import static com.android.volley.VolleyLog.v;

/**
 * Created by nusrat-pc on 12/6/16.
 */
public class NewUser_Activity_ extends Activity implements View.OnClickListener {

    private static final String TAG = NewUser_Activity_.class.getSimpleName();
    String gender, check;
    Button btn_catagory;
    EditText fromDateEtxt;
    private Context context = NewUser_Activity_.this;
    private EditText inputName;
    private EditText inputBirthDate;
    private EditText inputHeight;
    private EditText inputNationality;
    private EditText inputPassport_Number;
    private EditText inputNID_Number;
    private EditText inputTrade_Name;
    private EditText inputPhone_Number;

    private EditText inputUser_Name;
    private EditText inputPassword;
    private EditText inputRePassword;

    private Button btnRegister;
    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private CheckBox check1, check2;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_user_create_layout);

        btnRegister = (Button) findViewById(R.id.btn_reg_submit);
        btn_catagory = (Button) findViewById(R.id.btn_catagory);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());
        //check box initilialization
        check1 = (CheckBox) findViewById(R.id.checkBox1);
        //check bos click evevnt
        check1.setOnClickListener(this);
        //check box initilialization
        check2 = (CheckBox) findViewById(R.id.checkBox2);
        //check bos click evevnt
        check2.setOnClickListener(this);

        //call find viw id method in on create view
        findViewsById();
        setDateTimeField();

        //Edit text find id--
        inputName = (EditText) findViewById(R.id.name);
        inputBirthDate = (EditText) findViewById(R.id.birth_date);
        inputHeight = (EditText) findViewById(R.id.height);
        inputNationality = (EditText) findViewById(R.id.nationality);
        inputPassport_Number = (EditText) findViewById(R.id.passport_no);
        inputNID_Number = (EditText) findViewById(R.id.nid_no);
        inputTrade_Name = (EditText) findViewById(R.id.trade_name);
        inputPhone_Number = (EditText) findViewById(R.id.phone_no);
        inputUser_Name = (EditText) findViewById(R.id.input_userName);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputRePassword = (EditText) findViewById(R.id.input_repassword);


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        // Login button Click Event
        btn_catagory.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                Intent intent = new Intent(NewUser_Activity_.this, JobCategory_Activity.class);
                startActivity(intent);

            }

        });


        // Registration button Click Event
        btnRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                ArrayList<String> dataset = new ArrayList<String>();

                final String name = inputName.getText().toString().trim();
                final String birthday = inputBirthDate.getText().toString().trim();
                final String height = inputHeight.getText().toString().trim();
                final String nationality = inputNationality.getText().toString().trim();
                final String passport_no = inputPassport_Number.getText().toString().trim();
                final String nid = inputNID_Number.getText().toString().trim();
                final String trade_name = inputTrade_Name.getText().toString().trim();
                final String phone_no = inputPhone_Number.getText().toString().trim();

                final String user_name = inputUser_Name.getText().toString().trim();
                final String password = inputPassword.getText().toString().trim();
                final String repassword = inputRePassword.getText().toString().trim();


                if (inputName.getText().toString().length() == 0) {
                    inputName.setError("Please enter your name");
                    inputName.requestFocus();
                }


               else if (inputHeight.getText().toString().length() == 0) {
                    inputHeight.setError("Please enter your height");
                    inputHeight.requestFocus();
                }


                else if (inputNationality.getText().toString().length() == 0) {
                    inputNationality.setError("Please enter your nationality");
                    inputNationality.requestFocus();
                }


                else if (inputPassport_Number.getText().toString().length() == 0) {
                    inputPassport_Number.setError("Please enter your passport number");
                    inputPassport_Number.requestFocus();
                }

                else   if (passport_no.length()!=9)
                {
                    Toast.makeText(getApplicationContext(), "Invalid passport number!", Toast.LENGTH_LONG).show();

                }



                else  if (inputNID_Number.getText().toString().length() == 0) {
                    inputNID_Number.setError("Please enter your NID");
                    inputNID_Number.requestFocus();
                }

                else  if (nid.length()!=13)
                {
                    Toast.makeText(getApplicationContext(), "Invalid NID!", Toast.LENGTH_LONG).show();

                }



                else  if (inputTrade_Name.getText().toString().length() == 0) {
                    inputTrade_Name.setError("Please enter trade name");
                    inputTrade_Name.requestFocus();
                }

                else  if (inputPhone_Number.getText().toString().length() == 0) {
                    inputPhone_Number.setError("Please enter your phone number");
                    inputPhone_Number.requestFocus();
                }





                else if (!isValidMobile(phone_no)) {
                    Toast.makeText(getApplicationContext(), "Phone number not valid!", Toast.LENGTH_LONG).show();

                }
              else   if ( (gender == null) ) {
                    Toast.makeText(getApplicationContext(), "Please select your gender", Toast.LENGTH_LONG).show();

                }
                else  if (inputUser_Name.getText().toString().length() == 0) {
                    inputUser_Name.setError("Please enter user name");
                    inputUser_Name.requestFocus();
                }
                else  if (inputPassword.getText().toString().length() == 0) {
                    inputPassword.setError("Please enter password");
                    inputPassword.requestFocus();
                }

                else  if (inputRePassword.getText().toString().length() == 0) {
                    inputRePassword.setError("Please re-enter password");
                    inputRePassword.requestFocus();
                }


                else if (!checkPassWordAndConfirmPassword(password, repassword)) {

                    Toast.makeText(getApplicationContext(), "Password don't match", Toast.LENGTH_LONG).show();
                }

                else if (!isNetworkAvailable(getApplicationContext())){
                    //information add to database-----
                    db.addUser(name, birthday, height, nationality, passport_no, nid, trade_name, phone_no, gender, user_name, password, repassword);

                   //network not available custom toast---
                    Context context = getApplicationContext();
                    LayoutInflater inflater = getLayoutInflater();

                    View customToastroot = inflater.inflate(R.layout.red_toast, null);

                    Toast customtoast = new Toast(context);

                    customtoast.setView(customToastroot);
                    customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                    customtoast.setDuration(Toast.LENGTH_LONG);
                    customtoast.show();

                }

                else {

                    db.addUser(name, birthday, height, nationality, passport_no, nid, trade_name, phone_no, gender, user_name, password, repassword);

                    dataSendToServer(name, birthday, height, nationality, passport_no, nid, trade_name, phone_no, gender, user_name, password, repassword);

                    //Custom toast for showing successful message

                    Context context = getApplicationContext();
                    LayoutInflater inflater = getLayoutInflater();

                    View customToastroot = inflater.inflate(R.layout.blue_toast, null);

                    Toast customtoast = new Toast(context);

                    customtoast.setView(customToastroot);
                    customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                    customtoast.setDuration(Toast.LENGTH_LONG);
                    customtoast.show();

                    Intent intent = new Intent(NewUser_Activity_.this, Login_Activity.class);
                    startActivity(intent);

                }

            }

        });


    }

    //Check password and repassword is write or wrong
    public boolean checkPassWordAndConfirmPassword(String password, String repassword) {
        boolean pstatus = false;
        if (repassword != null && password != null) {
            if (password.equals(repassword)) {
                pstatus = true;
            }
        }
        return pstatus;
    }

    //Validate phone number--
    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

/*
Male and Female radio button listioner
 */

    public void radioClick(View v) {
        boolean checked = ((RadioButton) v).isChecked();

        switch (v.getId()) {
            case R.id.radio_male_bt:
                if (checked) {
                    gender = "male";
                }
                break;

            case R.id.radio_female_bt:
                if (checked) {
                    gender = "female";
                }
                break;
            default:
                break;
        }

    }


    /*
    find the birthday day from edit text
     */

    private void findViewsById() {
        fromDateEtxt = (EditText) findViewById(R.id.birth_date);
        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();


    }

    /*
    set birthdate
     */
    private void setDateTimeField() {
        fromDateEtxt.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    /*
    Birth date on click
     */

    @Override
    public void onClick(View v) {

        if (v == fromDateEtxt)
            fromDatePickerDialog.show();

        //check the checkbox if yes or not
        switch (v.getId()) {
            case R.id.checkBox1:
                if (check1.isChecked()) {
                    check = "trick1";
                    Toast.makeText(getApplicationContext(), "Agreed", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Disagreed", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.checkBox2:
                if (check2.isChecked()) {
                    check = "trick2";
                    Toast.makeText(getApplicationContext(), "Subcribed", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Unsubcribed", Toast.LENGTH_LONG).show();

                }
                break;

        }
    }

    //Sending data to server-------------
    private void dataSendToServer(String name, String birthday, String height, String nationality, String passport_no, String nid, String trade_name, String phone_no, String gender,String user_name,String password,String repassword) {

        String hitURL = " http://192.168.1.131:8080/baseApi/web/users";

        HashMap<String, String> params = new HashMap<>();
        params.put("name", name); //Items - Item 2 - lnamess
        params.put("age", birthday); //Items - Item 3
        params.put("height", height);
        params.put("nationality", nationality);
        params.put("passport_number", passport_no);
        params.put("nid", nid);
        params.put("trade_name", trade_name);
        params.put("phone_no", phone_no);
        params.put("gender", gender);
        params.put("user_name", user_name);
        params.put("password", password);
        params.put("repassword", repassword);


        pDialog.setMessage("Registration Processing ...");
        showDialog();

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d(TAG, "Register Response: " + response.toString());

                        hideDialog();


                        try {


                            v("Response:%n %s", response.toString(4));


                            if (response != null) {

                                //Custom toast for showing successful message

                                Context context = getApplicationContext();
                                LayoutInflater inflater = getLayoutInflater();

                                View customToastroot = inflater.inflate(R.layout.blue_toast, null);

                                Toast customtoast = new Toast(context);

                                customtoast.setView(customToastroot);
                                customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                                customtoast.setDuration(Toast.LENGTH_LONG);
                                customtoast.show();
                            }

                            else {

                                Context context = getApplicationContext();
                                LayoutInflater inflater = getLayoutInflater();

                                View customToastroot = inflater.inflate(R.layout.yellow_toast, null);

                                Toast customtoast = new Toast(context);

                                customtoast.setView(customToastroot);
                                customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
                                customtoast.setDuration(Toast.LENGTH_LONG);
                                customtoast.show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ServerError) {
                            Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();


                        } else if (volleyError instanceof ParseError) {
                            Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                        }


                    }
                });


        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(postRequest);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * @param context get the applicationcontext
     * @return network is available or not
     */

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}


