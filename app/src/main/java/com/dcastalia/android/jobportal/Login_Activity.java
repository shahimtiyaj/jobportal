package com.dcastalia.android.jobportal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dcastalia.android.jobportal.DatabaseHelper.SQLiteHandler;

public class Login_Activity extends Activity {

    Button btn_login;
    Button btn_NewUser;

    private EditText inputUser_Name;
    private EditText inputPassword;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        btn_login = (Button) findViewById(R.id.btn_login);
        btn_NewUser = (Button) findViewById(R.id.btn_NewUser);

        inputUser_Name = (EditText) findViewById(R.id.input_userName);
        inputPassword = (EditText) findViewById(R.id.input_password);
        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Login button Click Event
        btn_login.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                String user_name = inputUser_Name.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!user_name.isEmpty() && !password.isEmpty()) {
                    // login user

                    if (db.checkUserPassword(user_name, password)) {
                        Toast.makeText(Login_Activity.this, "Logging....", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(Login_Activity.this, Profile_Activity.class));
                    }
                    else {

                        Toast.makeText(Login_Activity.this, "Wrong information!", Toast.LENGTH_SHORT).show();

                    }
                }

                else  if (inputUser_Name.getText().toString().length() == 0) {
                    inputUser_Name.setError("Please enter user name");
                    inputUser_Name.requestFocus();
                }
                else  if (inputPassword.getText().toString().length() == 0) {
                    inputPassword.setError("Please enter password");
                    inputPassword.requestFocus();
                }



            }

        });

        //  Reg button Click Event
        btn_NewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Login_Activity.this, NewUser_Activity_.class);
                startActivity(intent);

            }
        });


    }
}

