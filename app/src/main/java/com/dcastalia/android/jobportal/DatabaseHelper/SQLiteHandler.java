package com.dcastalia.android.jobportal.DatabaseHelper;

/**
 * Created by nusrat-pc on 10/19/16.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHandler extends SQLiteOpenHelper {

    // Login Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_DATE = "date";
    public static final String KEY_HEIGHT = "height";
    public static final String KEY_NATIONALITY = "nationality";
    public static final String KEY_PASSPORT_NO = "passport_no";
    public static final String KEY_NID = "nid";
    public static final String KEY_TRADE_NAME = "trade_name";
    public static final String KEY_PHONE_NO = "phone_no";
    public static final String KEY_GENDER = "gender";
    //User reg---------------------------
    public static final String KEY_USER_NAME = "user_name";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_RE_PASSWORD = "repassword";



    private static final String TAG = SQLiteHandler.class.getSimpleName();
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 4;
    // Database Name
    private static final String DATABASE_NAME = "SampleDB";
    // Login table name
    private static final String TABLE_USER = "User";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) throws SQLException {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_DATE + " date,"
                + KEY_HEIGHT + " TEXT,"
                + KEY_NATIONALITY + " TEXT,"
                + KEY_PASSPORT_NO + " TEXT,"
                + KEY_NID + " TEXT,"
                + KEY_TRADE_NAME + " TEXT,"
                + KEY_PHONE_NO + " TEXT,"
                + KEY_GENDER + " TEXT,"
                + KEY_USER_NAME + " TEXT,"
                + KEY_PASSWORD + " TEXT,"
                + KEY_RE_PASSWORD + " TEXT "
                + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing User details in database
     */
    public long addUser(String name, String date, String height, String nationality, String passport_no, String nid, String trade_name, String phone_no, String gender,String user_name,String password,String repassword) {
        {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_NAME, name); // Name
            values.put(KEY_DATE, date); // date
            values.put(KEY_HEIGHT, height); //height
            values.put(KEY_NATIONALITY, nationality);
            values.put(KEY_PASSPORT_NO, passport_no);
            values.put(KEY_NID, nid);
            values.put(KEY_TRADE_NAME, trade_name);
            values.put(KEY_PHONE_NO, phone_no);
            values.put(KEY_GENDER, gender);
            values.put(KEY_USER_NAME,user_name);
            values.put(KEY_PASSWORD, password);
            values.put(KEY_RE_PASSWORD, repassword);

            // Inserting Row
            long id = db.insert(TABLE_USER, null, values);
            db.close(); // Closing database connection

            Log.d(TAG, "New User inserted into sqlite: " + id);

            return id;
        }
    }

    public boolean checkUserPassword(String user_name, String password) {
        boolean check = false;

        String query = "SELECT * FROM " + TABLE_USER;

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                if (user_name.equals(cursor.getString(cursor.getColumnIndex(KEY_USER_NAME)))
                        && password.equals(cursor.getString(cursor.getColumnIndex(KEY_PASSWORD)))) {
                    check = true;
                    break;
                }
            } while (cursor.moveToNext());
        }

        return check;
    }


    public void updateEntry(String passport_no, String phone_no) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues updatedValues = new ContentValues();
        updatedValues.put(KEY_PASSPORT_NO ,passport_no);
        updatedValues.put(KEY_PHONE_NO, phone_no);

        String where = "PASSPORT_NO = ?";
        db.update(TABLE_USER, updatedValues, where, new String[] { passport_no });
    }


    /**
     * Re create database Delete all tables and create them again
     */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all User info from sqlite");
    }

}
