package com.dcastalia.android.jobportal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by nusrat-pc on 12/6/16.
 */
public class JobCategory_Activity extends Activity {

    private Context context = JobCategory_Activity.this;
    private static final String TAG = JobCategory_Activity.class.getSimpleName();

    private EditText inputName;
    private EditText inputBirthDate;
    private EditText inputHeight;
    private EditText inputNationality;
    private EditText inputPassport_Number;
    private EditText inputNID_Number;
    private EditText inputTrade_Name;
    private EditText inputPhone_Number;

    private Button btnRegister;

    private ProgressDialog pDialog;

    ListView listview ;
    String[] ListViewItems = new String[] {
            "ACCOUNTING/FINANCE",
            "PLANT/ANIMAL/FISHERIES",
            "BANK/NON-BANK FIN.INSTITUTION",
            "BEAUTY CARE/HEALTH/FITNESS",
            "CUSTOMER SUPPORT/CALL CENTER",
            "DATA ENTRY/OPERATOR/BPO",
            "DESIGN/CREATIVE",
            "DRIVING/MOTOR TECHNICIAN",
            "EDUCATION/TRAINING",
            "ELECTRICIAN/CONSTRUCTION/REPAIR",
            "ENGINEER/ARCHITECT",
            "GENERAL MANAGEMENT/ADMIN",
            "HOSPITALITY/TRAVEL/TOURISM",
            "HR/ORG.DEVELOPMENT",
            "IT/TELECOMMUNICATION"

    };


    SparseBooleanArray sparseBooleanArray ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.catagory_listview);

        listview = (ListView)findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getApplicationContext(),
                        android.R.layout.simple_list_item_multiple_choice,
                        android.R.id.text1, ListViewItems );


        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                sparseBooleanArray = listview.getCheckedItemPositions();

                String ValueHolder = "" ;

                int i = 0 ;

                while (i < sparseBooleanArray.size()) {

                    if (sparseBooleanArray.valueAt(i)) {

                        ValueHolder += ListViewItems [ sparseBooleanArray.keyAt(i) ] + ",";
                    }

                    i++ ;
                }

                ValueHolder = ValueHolder.replaceAll("(,)*$", "");

                Toast.makeText(JobCategory_Activity.this, "ListView Selected Values = " + ValueHolder, Toast.LENGTH_LONG).show();

            }
        });

    }
}
